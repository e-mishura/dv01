import scala.concurrent.duration._

object Solution extends App {

  case class CallRecord(phone: String, duration: FiniteDuration)

  case class PhoneTotal(phone: String, charge: Int, duration: FiniteDuration) {
    def +(other: PhoneTotal): PhoneTotal = {
      assert(phone == other.phone)
      PhoneTotal(phone, charge + other.charge, duration + other.duration)
    }
  }

  def solution(s: String): Int = {
    val log = s
      .lines
      .toStream
      .map(parseCallRecord)
      .map(calculateCallCharge)
      .groupBy(_.phone)
      .mapValues(_.reduce(_ + _))
      .values
      .toStream
      .sortBy(pt => (pt.duration, pt.phone.split('-').last)) //ascending
      .init  //drop last one which has promotion


    log.foreach(println)
    log.map(_.charge)
      .sum
  }

  val dpattern = raw"(\d{2}):(\d{2}):(\d{2})".r

  def parseCallRecord(rec: String): CallRecord = {
    val parts = rec.split(',')
    val duration: FiniteDuration = parts(0) match {
      case dpattern(hr, min, sec) =>
        FiniteDuration(hr.toLong, HOURS) +
        FiniteDuration(min.toLong, MINUTES) +
        FiniteDuration(sec.toLong, SECONDS)
      case _ => throw new Exception(s"Invalid duration format in ${parts(0)}")
    }
    CallRecord(parts(1), duration)
  }

  def calculateCallCharge(call: CallRecord): PhoneTotal = {

    val charge: Int =
      if(call.duration < 5.minutes)
        3 * call.duration.toSeconds.toInt
      else {
        val fullMinutesCharge = 150 * call.duration.toMinutes.toInt
        val partial = call.duration.toSeconds % 60
        if(partial > 0)
          fullMinutesCharge + 150
        else
          fullMinutesCharge
      }
    PhoneTotal(call.phone, charge, call.duration)
  }

  val input =
    """00:01:07,400-234-090
      |00:05:01,701-080-080
      |00:05:00,400-234-090
      |00:05:04,701-080-188
      |00:05:04,701-080-089
      |00:05:04,701-080-001""".stripMargin

  val s = solution(input)
  println(s)

}
